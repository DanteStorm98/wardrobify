from django.db import models
#from django.urls import reverse
# Create your models here.


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()


class Shoes(models.Model):
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=100)
    manufacturer = models.CharField(max_length=50)
    picture = models.URLField(
        max_length=500,
        null=True
    )

    bin = models.ForeignKey(
        "BinVO",
        related_name="bins",
        on_delete=models.CASCADE
    )
