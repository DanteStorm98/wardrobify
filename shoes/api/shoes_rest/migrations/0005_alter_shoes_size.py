# Generated by Django 4.0.3 on 2023-08-30 23:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0004_alter_shoes_options_alter_shoes_picture'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shoes',
            name='size',
            field=models.IntegerField(null=True),
        ),
    ]
