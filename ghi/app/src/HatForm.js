import React, { useEffect, useState } from 'react';

function HatForm () {
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.fabric = fabric;
        data.style_name = styleName;
        data.color = color;
        data.picture_url = pictureUrl
        data.location = selectedLocation;

        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();

            setFabric('');
            setStyleName('');
            setColor('');
            setPictureUrl('');
            setSelectedLocation('');
        }
    }

    const [fabric, setFabric] = useState('');

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const [styleName, setStyleName] = useState('');

    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }

    const [color, setColor] = useState('');

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const [pictureUrl, setPictureUrl] = useState('');

    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const [selectedLocation, setSelectedLocation] = useState('');

    const handleSelectedLocationChange = (event) => {
        const value = event.target.value;
        setSelectedLocation(value);
    }

    const [locations, setLocations] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8100/api/locations/";

        const response = await fetch(url);

        if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a hat</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input onChange={handleStyleNameChange} placeholder="Style Name" required type="text" value={styleName} name="style_name" id="style_name" className="form-control"/>
              <label htmlFor="name">Style name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFabricChange} placeholder="Fabric" required type="text" value={fabric} name="fabric" id="fabric" className="form-control"/>
              <label htmlFor="Fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleColorChange} placeholder="Color" required type="text" value={color} name="color" id="color" className="form-control"/>
              <label htmlFor="city">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePictureUrlChange} placeholder="Image" required type="url" value={pictureUrl} name="picture_url" id="picture_url" className="form-control"/>
              <label htmlFor="city">Image</label>
            </div>
            <div className="mb-3">
              <select onChange={handleSelectedLocationChange} required id="location" value={selectedLocation} name="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>
                        {location.closet_name}
                    </option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Add</button>
          </form>
        </div>
      </div>
    </div>
    );
}

export default HatForm;
