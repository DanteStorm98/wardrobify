import React, { useEffect, useState } from "react";

function ShoeForm () {
  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};

    data.name = name;
    data.color = color;
    data.manufacturer = manufacturer;
    data.picture = picture;
    data.bin = selectedBin;

    const shoeUrl = "http://localhost:8080/api/shoes/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    }

    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();

      setName('');
      setColor('');
      setPicture('');
      setSelectedBin('');
      setManufacturer('');
    }
  }

  const [manufacturer, setManufacturer] = useState('');
  const handleManufacturerChange = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  }

  const [name, setName] = useState('');
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const [color, setColor] = useState('');
  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
  }

  const[picture, setPicture] = useState('');
  const handlePictureChange = (event) => {
    const value = event.target.value;
    setPicture(value);
  }

  const [selectedBin, setSelectedBin] = useState('');
  const handleSelectedBinChange = (event) => {
    const value = event.target.value;
    setSelectedBin(value);
  }

  const [bins, setBins] = useState([]);

  const fetchData = async () => {
    const url = "http://localhost:8100/api/bins";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
      console.log("DATA:", data)
    }
  };
    useEffect(() => {
      fetchData();
    }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create new Shoes</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} placeholder="Name" value={name} required type="text" name="name" id="name" className="form-control"/>
              <label htmlFor="name">name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleManufacturerChange} placeholder="manufacturer" value={manufacturer} required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleColorChange} placeholder="Color" value={color} required type="text" name="color" id="color" className="form-control"/>
              <label htmlFor="city">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePictureChange} placeholder="picture" value={picture} required type="url" name="picture" id="picture" className="form-control"/>
              <label htmlFor="city">Image</label>
            </div>
            <div className="mb-3">
              <select onChange={handleSelectedBinChange} required name="bin" id="bin" value={selectedBin} className="form-select">
                <option value="">Choose a closet</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.id} value={bin.id}>
                        {bin.closet_name}
                    </option>
                  )
                })}
              </select>
            </div>
            <button onClick={handleSubmit} className="btn btn-primary">Add</button>
          </form>
        </div>
      </div>
    </div>
    );
}

export default ShoeForm;
