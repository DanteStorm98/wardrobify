import { Link } from "react-router-dom";
import React, { useEffect, useState } from "react";


function ShoesList() {
  const [shoes, setShoes] = useState([]);
  const fetchData = async () => {
    const url = "http://localhost:8080/api/shoes/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);
    }
  };

  const handleDeleteShoe = (id) => {

    if (window.confirm("Are you sure?")) {
      fetch(`http://localhost:8080/api/shoes/${id}`, { method: "DELETE" })
        .then(() => {
          window.location.reload();
        })
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>

          </tr>
        </thead>
        <tbody>
        <div className="row row-cols-1 row-cols-md-3 g-4">
          {shoes.map((shoe) => {
            return (
              <tr key={shoe.id}>
              <div className="card h-100">
                <img
                  src={shoe.picture}
                  className="card-img-top"
                  alt={shoe.name}/>
                  <div className="card-body">
                    <h5 className="card-title">{shoe.name}</h5>
                    <p className="card-text">{shoe.bin}</p>
                  </div>
                <td>
                  <button
                    onClick={() => {
                      handleDeleteShoe(shoe.id);
                    }}
                    className="btn btn-info"
                  >
                    DELETE
                  </button>
                </td>
              </div>
              </tr>
            );
          })}
        </div>
        </tbody>
      </table>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
        <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">
          Add new shoes
        </Link>
      </div>
    </div>
  );
}

export default ShoesList;
